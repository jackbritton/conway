CC=g++
CFLAGS=-lsfml-graphics -lsfml-window -lsfml-system
TARGET=conway

main :
	$(CC) $(CFLAGS) *.c *.cpp -o $(TARGET)
