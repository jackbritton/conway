#include "Buffer.h"

Buffer Buffer_construct(unsigned int w, unsigned int h) {
    Buffer b;
    b.width = w;
    b.height = h;
    
    b.matrix = new bool*[w];
    for (unsigned int x = 0; x < w; x++) {
        b.matrix[x] = new bool[h];
        for (unsigned int y = 0; y < h; y++) {
            b.matrix[x][y] = false;
        }
    }
    return b;
}

Buffer Buffer_construct(unsigned int w, unsigned int h, bool startVal) {
    Buffer b;
    b.width = w;
    b.height = h;
    
    b.matrix = new bool*[w];
    for (unsigned int x = 0; x < w; x++) {
        b.matrix[x] = new bool[h];
        for (unsigned int y = 0; y < h; y++) {
            b.matrix[x][y] = startVal;
        }
    }
    return b;
}

Buffer Buffer_construct(unsigned int w, unsigned int h, double proportion) {
    Buffer b;
    b.width = w;
    b.height = h;
    unsigned int chance = proportion * RAND_MAX;
    
    b.matrix = new bool*[w];
    for (unsigned int x = 0; x < w; x++) {
        b.matrix[x] = new bool[h];
        for (unsigned int y = 0; y < h; y++) {
            if (rand() < chance) {
                b.matrix[x][y] = true;
            } else {
                b.matrix[x][y] = false;
            }
        }
    }
    return b;
}

Buffer Buffer_construct(Buffer b) {
    Buffer out;
    out.width = b.width;
    out.height = b.height;
    out.matrix = new bool*[out.width];
    for (unsigned int x = 0; x < out.width; x++) {
        out.matrix[x] = new bool[out.height];
        for (unsigned int y = 0; y < out.height; y++) {
            out.matrix[x][y] = b.matrix[x][y];
        }
    }
    return out;
}

Buffer Buffer_construct(Buffer in, unsigned int width, unsigned int height) {
    if (in.width == width && in.height == height) {
        return Buffer_construct(in);
    }
    //new buffer
    Buffer out = Buffer_construct(width,height);
    //determine the minimum dimensions
    unsigned int mWidth;
    unsigned int mHeight;
    if (width < in.width) {
        mWidth = width;
    } else {
        mWidth = in.width;
    }
    if (height < in.height) {
        mHeight = height;
    } else {
        mHeight = in.height;
    }

    //copy from in to out
    for (unsigned int x = 0; x < mWidth; x++) {
        for (unsigned int y = 0; y < mHeight; y++) {
            out.matrix[x][y] = in.matrix[x][y];
        }
    }

    return out;
}

Buffer Buffer_construct(Buffer in, bool rotate) {
    if (!rotate) {
        return Buffer_construct(in);
    }

    Buffer out;
    unsigned int w;
    unsigned int h;
    w = in.height;
    h = in.width;
    out.width = w;
    out.height = h;
    out.matrix = new bool*[out.width];
    for (unsigned int x = 0; x < out.width; x++) {
        out.matrix[x] = new bool[out.height];
        for (unsigned int y = 0; y < out.height; y++) {
            out.matrix[x][y] = in.matrix[y][x];
        }
    }

    return out;
}

void Buffer_destroy(Buffer* b) {
    for (unsigned int x = 0; x < b->width; x++) {
        delete[] b->matrix[x];
    }
    delete[] b->matrix;
}

bool Buffer_copy(Buffer read, Buffer* write) {
    if (read.width != write->width || read.height != write->height) {
        //error
        return false;
    }
    for (unsigned int x = 0; x < read.width; x++) {
        for (unsigned int y = 0; y < read.height; y++) {
            write->matrix[x][y] = read.matrix[x][y];
        }
    }
    return true;
}
//insert one buffer into another buffer
bool Buffer_insert(Buffer child, Buffer* parent, unsigned int x, unsigned int y) {
    //if it won't fit
    if (x+child.width >= parent->width || y+child.height >= parent->height) {
        return false;
    }
    //if mouse position is out of bounds
    if (x < 0 || x >= parent->width || y < 0 || y >= parent->height) {
        return false;
    }
    for (unsigned int i = 0; i < child.width; i++) {
        for (unsigned int j = 0; j < child.height; j++) {
            parent->matrix[x+i][y+j] = child.matrix[i][j];
        }
    }
    return true;
}
