#ifndef BUFFER_H
#define BUFFER_H

#include <stdlib.h>

typedef struct {
    bool** matrix;
    unsigned int width;
    unsigned int height;
} Buffer;

//grid defaults to all false
Buffer Buffer_construct(unsigned int w, unsigned int h);
//start with the entire grid being one value
Buffer Buffer_construct(unsigned int w, unsigned int h, bool startVal);
//start with a random chance of being dead or alive
Buffer Buffer_construct(unsigned int w, unsigned int h, double proportion);
//init from other buffer
Buffer Buffer_construct(Buffer b);
//resize from another buffer
Buffer Buffer_construct(Buffer in, unsigned int width, unsigned int height);
//rotate
Buffer Buffer_construct(Buffer in, bool rotate);

void Buffer_destroy(Buffer* b);

//copy buffers
bool Buffer_copy(Buffer read, Buffer* write);
//insert one buffer into another buffer
bool Buffer_insert(Buffer child, Buffer* parent, unsigned int x, unsigned int y);
#endif
