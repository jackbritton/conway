#include <stdio.h>
#include <cstdlib>
#include <string>
#include <fstream>
#include <SFML/Graphics.hpp>
#include "Buffer.h"

void step(Buffer read, Buffer* write, bool surviveConditions[8], bool birthConditions[8]);
//n is the number of living neighbors
bool stepCellValue(bool cellValue, unsigned char n, bool surviveConditions[8], bool birthConditions[8]); sf::Color mixColors(sf::Color a, sf::Color b, float proportion); void render(Buffer buffer, sf::RenderWindow* window, double cellWidth, double cellHeight, sf::Color startColor, sf::Color endColor);

//new
void saveToFile(std::string name, Buffer buffer, unsigned int x, unsigned int y, unsigned int width, unsigned int height);
void loadFromFile(std::string name, Buffer* buffer);

int main(void) {
    //options
    bool PAUSED = true;
    unsigned int FRAMERATE;
    unsigned int WIDTH;
    unsigned int HEIGHT;
    //survive / birth conditions
    bool SURVIVE_CONDITIONS[8];
    bool BIRTH_CONDITIONS[8];
    for (char i = 0; i < 8; i++) {
        BIRTH_CONDITIONS[i] = false;
        SURVIVE_CONDITIONS[i] = false;
    }
    srand(time(0));

    printf("Width: ");
    scanf("%u", &WIDTH);
    printf("Height: ");
    scanf("%u", &HEIGHT);
    printf("Framerate: ");
    scanf("%u", &FRAMERATE);

    //get survive/birth conditions
    printf("Survive/Birth Conditions (ex. 23/3): ");
    int sNum, bNum;
    scanf("%d/%d", &sNum, &bNum);
    while (sNum > 0) {
        int v = sNum % 10;
        SURVIVE_CONDITIONS[v] = true;
        sNum /= 10;
    }
    while (bNum > 0) {
        int v = bNum % 10;
        BIRTH_CONDITIONS[v] = true;
        bNum /= 10;
    }

    //world and world buffer
    Buffer WORLD = Buffer_construct(WIDTH,HEIGHT);
    Buffer WORLD_BUFFER = Buffer_construct(WORLD);
    Buffer PASTE_BUFFER = Buffer_construct(4,4,0.5);

	sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "Conway's Game of Life");
	window.setFramerateLimit(FRAMERATE);

    //get the change in x / y from square to square
    sf::Vector2u windowSize = window.getSize();
    double CELL_WIDTH = (double) window.getSize().x / (double) WIDTH;
    double CELL_HEIGHT = (double) window.getSize().y / (double) HEIGHT;

    //colors
    sf::Color startColor(127,0,255);
    sf::Color endColor(255,127,127);

    while (window.isOpen()) {
        //mouse position
        sf::Vector2f mousePosition = window.mapPixelToCoords(sf::Mouse::getPosition(window),window.getDefaultView());
        unsigned int mouseX = mousePosition.x / CELL_WIDTH;
        unsigned int mouseY = mousePosition.y / CELL_HEIGHT;
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
            }
            //key events
			if (event.type == sf::Event::KeyPressed) {
                //Space for Pause/Play
				if (event.key.code == sf::Keyboard::Space) {
                    PAUSED = !PAUSED;
				}
                //N for Next
				if (event.key.code == sf::Keyboard::N) {
                    if (PAUSED) {
                        step(WORLD, &WORLD_BUFFER, SURVIVE_CONDITIONS, BIRTH_CONDITIONS);
                    }
				}
                //P for Paste
				if (event.key.code == sf::Keyboard::P) {
                    //adjust coordinates so we're drawing the center
                    unsigned int x = mouseX - (PASTE_BUFFER.width/2);
                    unsigned int y = mouseY - (PASTE_BUFFER.height/2);
                    if (!Buffer_insert(PASTE_BUFFER, &WORLD_BUFFER, x, y)) {
                        printf("Failed to insert buffer, it won't fit!\n");
                    }
				}
                //S for Save
				if (event.key.code == sf::Keyboard::S) {
                    char name[21];
                    printf("File to save buffer to (20 chars max): ");
                    scanf("%s", name);
                    saveToFile(name, WORLD, 0,0,WIDTH,HEIGHT);
				}
                //L for Load
				if (event.key.code == sf::Keyboard::L) {
                    char name[21];
                    printf("File to load buffer from (20 chars max): ");
                    scanf("%s", name);
                    loadFromFile(name, &PASTE_BUFFER);
				}
                //R for Resize
				if (event.key.code == sf::Keyboard::R) {
                    unsigned int w;
                    unsigned int h;
                    printf("Resizing!\nWidth: ");
                    scanf("%u", &w);
                    printf("Height: ");
                    scanf("%u", &h);

                    //create new buffers and destroy the old ones
                    Buffer temp1 = Buffer_construct(WORLD, w, h);
                    Buffer temp2 = Buffer_construct(WORLD_BUFFER, w, h);
                    Buffer_destroy(&WORLD);
                    Buffer_destroy(&WORLD_BUFFER);
                    WORLD = temp1;
                    WORLD_BUFFER = temp2;

                    WIDTH = w;
                    HEIGHT = h;
                    CELL_WIDTH = (double) window.getSize().x / (double) WIDTH;
                    CELL_HEIGHT = (double) window.getSize().y / (double) HEIGHT;
				}
                //T for roTate
				if (event.key.code == sf::Keyboard::T) {
                    //create new buffers and destroy the old ones
                    Buffer temp = Buffer_construct(PASTE_BUFFER);
                    PASTE_BUFFER = Buffer_construct(temp,true);
				}
			}
		}

        //update
        if (!PAUSED) {
            step(WORLD, &WORLD_BUFFER, SURVIVE_CONDITIONS, BIRTH_CONDITIONS);
        }
        //mouse input
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (mouseX >= 0 && mouseX < WIDTH && mouseY >= 0 && mouseY < HEIGHT) {
                WORLD_BUFFER.matrix[mouseX][mouseY] = true;
            }
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
            if (mouseX >= 0 && mouseX < WIDTH && mouseY >= 0 && mouseY < HEIGHT) {
                WORLD_BUFFER.matrix[mouseX][mouseY] = false;
            }
		}

        //copy the world buffer to the world
        Buffer_copy(WORLD_BUFFER, &WORLD);

		window.clear(sf::Color::Black);

		//draw everything here...
        render(WORLD, &window, CELL_WIDTH, CELL_HEIGHT, startColor, endColor);
		
		window.display();
	}
}

void step(Buffer read, Buffer* write, bool surviveConditions[8], bool birthConditions[8]) {
    //if the dimensions are wrong, fail
    if (read.width != write->width || read.height != write->height) {
        printf("Error in step(): Buffer args do not have the same dimensions!\n");
        return;
    }
    //relative coordinates to neighbors
    struct {
        char x;
        char y;
    } relCoords[8] = {
        {-1,-1},
        {-1, 0},
        {-1, 1},
        { 0,-1},
        { 0, 1},
        { 1,-1},
        { 1, 0},
        { 1, 1}
    };

    //for each cell..
    for (unsigned int x = 0; x < read.width; x++) {
        for (unsigned int y = 0; y < read.height; y++) {
            //count living neighbors
            unsigned char n = 0;
            for (unsigned int i = 0; i < 8; i++) {
                //wrap coordinates
                int xn = x+relCoords[i].x;
                int yn = y+relCoords[i].y;
                xn += read.width;
                xn %= read.width;
                yn += read.height;
                yn %= read.height;

                //check those spaces.. if true, increment n
                if (read.matrix[xn][yn]) {
                    n++;
                }
            }

            //interpret the number of living neighbors
            if (read.matrix[x][y]) {
                if (surviveConditions[n]) {
                    write->matrix[x][y] = true;
                } else {
                    write->matrix[x][y] = false;
                }
            } else {
                if (birthConditions[n]) {
                    write->matrix[x][y] = true;
                } else {
                    write->matrix[x][y] = false;
                }
            }
        }
    }
}

bool stepCellValue(bool cellValue, unsigned char n, bool surviveConditions[8], bool birthConditions[8]) {
    if (cellValue) {
        if (surviveConditions[n]) {
            return true;
        }
    } else {
        if (birthConditions[n]) {
            return true;
        }
    }
    return false;
}

sf::Color mixColors(sf::Color a, sf::Color b, float proportion) {
    unsigned char red = (a.r-b.r)*proportion + b.r;
    unsigned char green = (a.g-b.g)*proportion + b.g;
    unsigned char blue = (a.b-b.b)*proportion + b.b;
    return sf::Color(red,green,blue);
}

void render(Buffer buffer, sf::RenderWindow* window, double cellWidth, double cellHeight, sf::Color startColor, sf::Color endColor) {
    //our rect
    sf::RectangleShape rect;
    rect.setSize(sf::Vector2f(cellWidth,cellHeight));

    //iterate and render!
    for (unsigned int x = 0; x < buffer.width; x++) {
        for (unsigned int y = 0; y < buffer.height; y++) {
            if (buffer.matrix[x][y]) {
                //color interpolation
                float proportion = ((float) x / buffer.width + (float) y / buffer.height) / 2;
                rect.setFillColor(mixColors(startColor,endColor,proportion));

                rect.setPosition(x*cellWidth,y*cellHeight);
                window->draw(rect);
            }
        }
    }
}

void saveToFile(std::string name, Buffer buffer, unsigned int x, unsigned int y, unsigned int width, unsigned int height) {
    std::ofstream file;
    file.open(name);
    file << width << std::endl;
    file << height << std::endl;
    for (unsigned int i = 0; i < width; i++) {
        for (unsigned int j = 0; j < height; j++) {
            file << buffer.matrix[x+i][y+j] << " ";
        }
        file << std::endl;
    }
    file.close();
    printf("Buffer \"%s\" saved!\n", name.c_str());
}

void loadFromFile(std::string name, Buffer* buffer) {
    std::ifstream file;
    file.open(name);
    if (file.is_open()) {
        //get the file's dimensions
        unsigned int fWidth;
        unsigned int fHeight;
        file >> fWidth;
        file >> fHeight;
        Buffer_destroy(buffer);
        *buffer = Buffer_construct(fWidth,fHeight);
        //init buffer
        for (unsigned int i = 0; i < fWidth; i++) {
            for (unsigned int j = 0; j < fHeight; j++) {
                file >> buffer->matrix[i][j];
            }
        }
        file.close();
        printf("Buffer \"%s\" loaded!\n", name.c_str());
    } else {
        printf("Error opening file \"%s\"\n", name.c_str());
    }
}
